from graphviz import Graph
from graphviz import Digraph

g = Graph(format='png')
dg = Digraph(format='png')

# 無向グラフ
# nodeを追加
g.node('1')
g.node('2')
g.node('3')
# edgeを追加
g.edge('1', '2')
g.edge('2', '3')
g.edge('2', '3')
g.edge('3', '1')

# 有向グラフ
dg.node('1', shape='box', color='darkorange1')
dg.node('2', shape='box', color='darkorange1')
dg.node('2', shape='box', color='darkorange1') # it is ok to double addition, results ignored.
dg.node('3\nport', shape='box', color='darkorange1')
dg.edge('1', '2', color='darkorange1')  # 1 -> 2
dg.edge('2', '3')  # 2 -> 3
dg.edge('2', '3')  # 2 -> 3
dg.edge('2', '2')  # 2 -> 2
dg.edge('3\nport', '1', style='dashed')  # 3 -> 1

dg.attr('graph', concentrate='true') # prevent node doubled.
g.render('./graph_test')
dg.render('./dgraph_test')
