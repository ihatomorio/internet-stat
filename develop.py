import subprocess
from graphviz import Digraph
import socket
import platform

MAX_HOSTS = 300
TRACE_TIMEOUT = 300

def enumerate_connection(limit = MAX_HOSTS):
    result_list = []
    # netstat
    if platform.system() == 'Windows':
        netstat_result = subprocess.run(["netstat", "-n"], capture_output=True).stdout.decode('cp932').splitlines()
    elif platform.system() == 'Darwin':
        netstat_result = subprocess.run(["netstat", "-n", "-p", "tcp"], capture_output=True).stdout.decode('utf8').splitlines()
    else:
        pass

    counta = 0
    for netstat_result_string in netstat_result:
        conn_data = netstat_result_string.split()
        print(conn_data)
        if platform.system() == 'Windows':
            if len(conn_data) < 3:
                continue
            local_address = conn_data[1]
            foreign_address = conn_data[2]
            state = conn_data[3]
            if len(foreign_address.split(':')) < 2:
                continue
            foreign_ip = foreign_address.split(':')[0]
            foreign_port = foreign_address.split(':')[1]
        elif platform.system() == 'Darwin':
            if len(conn_data) < 6:
                continue
            local_address = conn_data[3]
            foreign_address = conn_data[4]
            state = conn_data[5]
            foreign_ip = '.'.join(foreign_address.split('.')[:-1])
            foreign_port = foreign_address.split('.')[-1]
            if ':' in foreign_ip:
                continue
        else:
            pass

        if '127.0.0.1' in local_address:
            continue
        if '[' in local_address:
            continue
        if not 'ESTABLISHED' in state:
            continue

        counta += 1
        if counta > limit:
            break
        result_list.append({'ip': foreign_ip, 'port': foreign_port, 'tcp_status':state})

    return result_list

def traceroute_host(host, root_host='This host'):
    result_list = [root_host]
    # traceroute to each host
    if platform.system() == 'Windows':
        tracert_result = subprocess.run(["tracert", "-d", "-w", str(TRACE_TIMEOUT), host], capture_output=True).stdout.decode('cp932').splitlines()
    elif platform.system() == 'Darwin':
        tracert_result = subprocess.run(["traceroute", "-n", "-w", "1", "-m", "30", host], capture_output=True).stdout.decode('utf8').splitlines()
    else:
        pass

    print(tracert_result)

    i = 1
    for tracert_result_string in tracert_result:
        # print(tracert_result_string)
        tracert_data = tracert_result_string.split()
        # print(tracert_data)

        if len(tracert_data) == 0:
            continue

        if tracert_data[0] != str(i):
            continue

        if platform.system() == 'Windows':
            nexthop = tracert_data[-1]
        elif platform.system() == 'Darwin':
            nexthop = tracert_data[1]
        else:
            pass

        if '要求がタイムアウトしました。' in nexthop or '*' in nexthop:
            print('trace timedout. BREAK')
            break

        result_list.append(nexthop)
        i += 1

        if nexthop == host:
            print('trace completed.')
            break
    
    return result_list

class DomainQuery:
    def __init__(self) -> None:
        self.fqdn_cache = dict()

    def query_fqdn(self, ip):
        if ip not in self.fqdn_cache:
            try:
                self.fqdn_cache[ip] = socket.gethostbyaddr(ip)[0]
            except:
                self.fqdn_cache[ip] = ip
        return self.fqdn_cache[ip]

def main():
    # target graph
    dg = Digraph(format='png')
    dg.attr('graph', concentrate='true')
    sg = Digraph('sub')
    sg.graph_attr.update(rank='max')

    fqdncache = DomainQuery()

    me_host = 'This host'
    dg.node(me_host, shape='box', color='darkorange1')

    connection_list = enumerate_connection()
    print(connection_list)

    for connection in connection_list:
        # traceroute to each host
        route_vector = traceroute_host(connection['ip'], root_host=me_host)
        print(route_vector)

        target_box_string = fqdncache.query_fqdn(connection['ip']) + '\n' + connection['port']

        former_host = me_host
        for router_ip in route_vector[1:-1]:
            router_fqdn = fqdncache.query_fqdn(router_ip)
            dg.node(router_fqdn, shape='box')
            dg.edge(former_host, router_fqdn)
            former_host = router_fqdn

        if route_vector[-1] != connection['ip']:
            sg.node(target_box_string, shape='box', color='darkorange1')
            dg.edge(former_host, target_box_string, style='dashed')
        else:
            dg.node(target_box_string, shape='box', color='darkorange1')
            dg.edge(former_host, target_box_string)

    dg.subgraph(sg)
    dg.render('./dgraph')


if __name__ == "__main__":
    main()
