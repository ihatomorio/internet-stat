"""http://www.graphviz.org/pdf/dotguide.pdf, Figure 20"""

import graphviz

g = graphviz.Digraph(format='png')
g.attr(compound='true')

with g.subgraph(name='cluster0') as c:
    # c.edges(['ab', 'ac', 'bd', 'cd'])
    c.edges(['ab', 'ac', 'bd'])
    c.edge('c', 'd')
    # c.edge('d', 'c')
    c.node('BC', label="BC\nff:ff:ff:ff:ff:ff", shape='box')
    c.edge('a', 'BC')

with g.subgraph(name='cluster1') as c:
    c.edges(['eg', 'ef'])

g.edge('b', 'f', lhead='cluster1')
g.edge('d', 'e')
g.edge('c', 'g', ltail='cluster0', lhead='cluster1')
g.edge('c', 'e', ltail='cluster0')
g.edge('d', 'h')

# g.view()
g.render('./cluster_edge')
